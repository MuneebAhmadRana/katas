﻿using System.Text;

namespace Katas.Kata8
{
    public class CaseConverter
    {
        public static string ToCamelCase(string word)
        {
            //splitting into string array
            string[] words = word.Split("_");
            for (int i = 0; i < words.Length; i++)
            {
                if (i != 0)
                {
                    //Looping through index 1 to final index and uppercasing the first letter
                    // While lower casing everything else
                    words[i] = words[i].Substring(0, 1).ToUpper() + words[i][1..].ToLower();
                }
            }
            string finalWord = "";
            foreach (string w in words)
            {
                //Appending to string value.
                finalWord += w;
            }
            return finalWord;
        }

        public static string ToSnakeCase(string word)
        {
            StringBuilder builder = new StringBuilder();
            foreach (char c in word)
            {
                if (char.ToUpper(c) == c)
                {
                    builder.Append("_");
                }
                builder.Append(c);
            }
            return builder.ToString().ToLower();
        }
    }
}