﻿namespace Katas.Kata1
{
    internal class PascalCaseConverter
    {
        public static string PascalCase(string word)
        {
            word = string.Join(" ", word);
            return word;
        }

        //public static string PascalCase(string word)
        //{
        //    word = RemovePunctuations(word)
        //    string[] array = word.Split(" ")
        //    array = FixCasing(array)
        //    word = string.Join("", array)
        //    return word
        //}

        //public static string RemovePunctuations(string word)
        //{
        //    return Regex.Replace(word, @"[^\w\s]", "").Trim()
        //}

        //public static string[] FixCasing(string[] words)
        //{
        //    string[] returnArray = new string[words.Length]
        //    for (int i = 0 i < words.Length i++){
        //        if(words[i].Length == 1)
        //        {
        //            returnArray[i] = words[i].ToUpper();
        //        }
        //        else if(words[i].Length >= 2)
        //        {
        //            string word = words[i].ToLower();
        //            word = char.ToUpper(word[0]) + word.Substring(1);
        //            returnArray[i] = word;
        //        }
        //    }
        //    return returnArray;
        //}
    }
}