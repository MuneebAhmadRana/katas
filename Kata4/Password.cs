﻿using System;

namespace Katas.Kata4
{
    internal class Password
    {
        public static bool OneLowerCase(string password)
        {
            //Looping through the password
            foreach (char character in password)
            {
                Console.WriteLine(character);
                //checking if the lowercase of a character is equal to that same character
                if (!char.IsNumber(character))
                {
                    if (char.ToLower(character) == character)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool OneUpperCase(string password)
        {
            //Looping through the password
            foreach (char character in password)
            {
                Console.WriteLine(character);
                //checking if the Uppercase of a character is equal to that same character
                if (!char.IsNumber(character))
                {
                    if (char.ToUpper(character) == character)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool LengthAteast8(string password)
        {
            if (password.Length < 8)
            {
                return false;
            }
            return true;
        }

        public static bool AtleastOneDigit(string password)
        {
            foreach (char c in password)
            {
                if (char.IsNumber(c))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool AtleastOneSpecialChar(string password)
        {
            foreach (char c in password)
            {
                if (!char.IsLetter(c) && !char.IsNumber(c))
                {
                    return true;
                }
            }
            return false;
        }
    }
}