﻿namespace Katas.Kata13
{
    public class RomanConverter
    {
        //public static int Convert(string romanNumerals)
        //{
        //    int sum = 0;
        //    for (int i = romanNumerals.Length - 1; i >= 0; i--)
        //    {
        //        int current = FindValue(romanNumerals[i]);
        //        int next = 0;
        //        if(i != 0)
        //        {
        //            next = FindValue(romanNumerals[i - 1]);
        //        }
        //        else
        //        {
        //            next = 0;
        //        }

        //        if (current > next)
        //        {
        //            sum += current - next;
        //        }
        //        else
        //        {
        //            sum += current;
        //        }
        //        Console.WriteLine("The current value is: " + current);
        //        Console.WriteLine("The next value is: " + next);
        //        Console.WriteLine("The sum value is: " + sum);

        //    }
        //    return sum;
        //}

        public static int Convert(string romanNumerals)
        {
            int sum = 0;
            for (int i = 0; i < romanNumerals.Length; i++)
            {
                int current = FindValue(romanNumerals[i]);
                int next;
                //Check which does not allow array out of bounds
                if (i != romanNumerals.Length - 1)
                {
                    next = FindValue(romanNumerals[i + 1]);
                }
                else
                {
                    next = 0;
                }

                if (next > current)
                {
                    sum += next - current;
                    i += 1;
                }
                else
                {
                    sum += current;
                }
            }
            return sum;
        }

        public static int FindValue(char c)
        {
            switch (c)
            {
                case 'I':
                    return 1;

                case 'V':
                    return 5;

                case 'X':
                    return 10;

                case 'L':
                    return 50;

                case 'C':
                    return 100;

                case 'D':
                    return 500;

                case 'M':
                    return 1000;

                default:
                    return 0;
            }
        }
    }
}