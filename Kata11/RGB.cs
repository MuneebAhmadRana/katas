﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Katas.Kata11
{
    public class RGB
    {


        public static bool IsCorrectStartAndEndRGB(string rgb)
        {
            string firstFourLetters = rgb.Substring(0, 4);
            string lastLetter = rgb.Substring(rgb.Length - 1);
            Console.WriteLine(firstFourLetters);
            Console.WriteLine(lastLetter);
            if(firstFourLetters != "rgb(" || lastLetter != ")")
            {
                Console.WriteLine("The provided string either does not start with rgb( or does not end with )");
                return false;
            }
            return true;
        }

        public static bool NumberOfCommasRGB(string rgb)
        {
            string[] postSplit = rgb.Split(",");
            if(postSplit.Length != 3)
            {
                Console.WriteLine("There were not enough commas for the string to be considered RGB");
                return false;
            } 
            else if (postSplit.Contains(""))
            {
                return false;
            }
            return true;
        }

        public static bool InvalidNumbersRGB(string rgb) 
        {
            string semiSub = rgb.Substring(0, rgb.Length-1);
            string substr = semiSub[4..];
            Console.WriteLine(substr);

            string[] numbers = substr.Split(",");
            List<bool> boolList = new List<bool>();
            foreach (string number in numbers)
            {
                if (number.Contains("%"))
                {
                    Console.WriteLine("Working with percentage");
                    try
                    {
                        string numberWithoutPercentage = number.Substring(0, number.Length - 1);
                        int percentageAmount = int.Parse(numberWithoutPercentage);
                        if (percentageAmount < 0 || percentageAmount > 100)
                        {
                            boolList.Add(false);
                        }
                        else
                        {
                            boolList.Add(true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("The percentage sign was not placed in the correct place");
                        Console.WriteLine(ex.Message);
                    }
                }
                else
                {
                    Console.WriteLine("Working with 255 values");
                    try
                    {
                        int numberInt = int.Parse(number);
                        if (numberInt >= 0 && numberInt <= 255)
                        {
                            boolList.Add(true);
                        }
                        else
                        {
                            boolList.Add(false);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("went in second try");
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            if (boolList.Count != 3)
            {
                Console.WriteLine("There must be 3 sets of numbers only");
                return false;
            }
            else if (boolList.Contains(false))
            {
                Console.WriteLine("One of the numbers do not match the rgb critera");
                return false;
            }
            return true;
        }

        public static bool IsValidRGB(string rgb)
        {
            List<bool> boolList = new List<bool>();
            boolList.Add(IsCorrectStartAndEndRGB(rgb));
            boolList.Add(NumberOfCommasRGB(rgb));
            boolList.Add(InvalidNumbersRGB(rgb));

            if (boolList.Contains(false))
            {
                return false;
            }
            return true;
        }

        public static bool IsCorrectStartAndEndRGBA(string rgba)
        {
            string firstFourLetters = rgba.Substring(0, 5);
            string lastLetter = rgba.Substring(rgba.Length - 1);
            Console.WriteLine(firstFourLetters);
            Console.WriteLine(lastLetter);
            if (firstFourLetters != "rgba(" || lastLetter != ")")
            {
                Console.WriteLine("The provided string either does not start with rgba( or does not end with )");
                return false;
            }
            return true;
        }

        public static bool NumberOfCommasRGBA(string rgba)
        {
            string[] postSplit = rgba.Split(",");
            if (postSplit.Length != 4)
            {
                Console.WriteLine("There were not enough commas for the string to be considered RGB");
                return false;
            }
            else if (postSplit.Contains(""))
            {
                return false;
            }
            return true;
        }

        public static bool InvalidNumbersRGBA(string rgba)
        {
            string semiSub = rgba.Substring(0, rgba.Length - 1);
            string substr = semiSub[5..];
            Console.WriteLine(substr);

            string[] numbers = substr.Trim().Split(",");
            List<bool> boolList = new List<bool>();

            for (int i = 0; i < numbers.Length; i++)
            {
                if (i != 3)
                {
                    try
                    {
                        int numberInt = int.Parse(numbers[i]);

                        if (numberInt >= 0 && numberInt <= 255)
                        {
                            boolList.Add(true);
                        }
                        else
                        {
                            boolList.Add(false);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else
                {
                    double numberInt = double.Parse(numbers[i]);
                    Console.WriteLine("Checking the decimal one");
                    if (numberInt > 1 || numberInt < 0)
                    {
                        boolList.Add(false);
                    }
                    else
                    {
                        boolList.Add(true);
                    }
                }
            }
            if (boolList.Count != 4)
            {
                Console.WriteLine("There must be 4 sets of numbers only");
                return false;
            }
            else if (boolList.Contains(false))
            {
                Console.WriteLine("One of the numbers do not match the rgba critera");
                return false;
            }
            return true;
        }

        public static bool IsValidRGBA(string rgba)
        {
            List<bool> boolList = new List<bool>();
            boolList.Add(IsCorrectStartAndEndRGBA(rgba));
            boolList.Add(NumberOfCommasRGBA(rgba));
            boolList.Add(InvalidNumbersRGBA(rgba));

            if (boolList.Contains(false))
            {
                return false;
            }
            return true;
        }

        public static bool IsValidRgbColour(string rgb)
        {
            if (rgb.StartsWith("rgb("))
            {
                return IsValidRGB(rgb);
            }
            else if (rgb.StartsWith("rgba("))
            {
                return IsValidRGBA(rgb);
            }
            else
            {
                return false;
            }
        }
    }
}
