﻿using System;
using System.Linq;

namespace Katas.Kata6
{
    public class Reducer
    {
        //THIS WORKED WELL
        public static int SumDigProd(params int[] numbers)
        {
            int numbersAdded = Add(numbers);
            int[] numbersAddedArray = numbersAdded.ToString().Select(c => Convert.ToInt32(c.ToString())).ToArray();
            int multiplied = Multiply(numbersAddedArray);
            while (multiplied >= 10)
            {
                multiplied = SumDigProd(multiplied);
            }
            return multiplied;
        }

        public static int Add(params int[] numbers)
        {
            int totalCount = 0;
            foreach (int number in numbers)
            {
                totalCount += number;
            }
            return totalCount;
        }

        public static int Multiply(params int[] numbers)
        {
            int totalCount = 1;
            foreach (int number in numbers)
            {
                totalCount *= number;
            }
            return totalCount;
        }
    }
}