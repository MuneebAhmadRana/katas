﻿using System;

namespace Katas
{
    internal class Kata2
    {
        public static int LargestDifference(int[] array)
        {
            int count = 0;
            int smallestNumber = 10000000;
            int largestNumber = 0;
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = array.Length - 1; j > i; j--)
                {
                    if (array[i] < array[j])
                    {
                        if (array[i] < smallestNumber)
                        {
                            smallestNumber = array[i];
                            largestNumber = 0;
                        }
                        if (array[j] > largestNumber)
                        {
                            largestNumber = array[j];
                        }
                        count = largestNumber - smallestNumber;
                    }
                }
            }

            findLargestPatterns(array, count);
            return count;
        }

        public static void findLargestPatterns(int[] array, int count)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int m = array.Length - 1; m > i; m--)
                {
                    if (array[i] + array[m] == count)
                    {
                        Console.WriteLine(array[i] + "(" + i + ")  +  " + array[m] + "(" + m + ")" + " = " + count);
                    }
                }
            }
        }
    }
}