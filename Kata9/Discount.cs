﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Katas.Kata9
{
    public class Discount
    {
        public static double Price(int [] array)
        {
            int booksCount = array.Length;
            HashSet<int> set = new HashSet<int>(array);
            int uniqueBooksCount = set.Count;
            double discount;
            switch (uniqueBooksCount)
            {
                case 1:
                    discount = 0;
                    break;
                case 2:
                    discount = 0.05;
                    break;
                    case 3:
                    discount = 0.10;
                    break;
                case 4:
                    discount = 0.20;
                    break;
                default:
                    discount = 0;
                    break;
            }
            int noDiscountUniqueBooks = uniqueBooksCount * 8;
            double discountUniqueBooks =  (noDiscountUniqueBooks - (noDiscountUniqueBooks * discount));
            double totalPrice = discountUniqueBooks + (8 * (booksCount - uniqueBooksCount));
            return totalPrice;
        }
    }
}
