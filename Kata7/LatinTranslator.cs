﻿using System.Text.RegularExpressions;

namespace Katas.Kata7
{
    public class LatinTranslator
    {
        public static bool IsVowel(char c)
        {
            string Vowels = "aeiou";
            if (Vowels.Contains(c))
            {
                return true;
            }
            return false;
        }

        public static string TranslateWord(string word)
        {
            word = word.ToLower();
            if (IsVowel(word[0]))
            {
                word += "yay";
                return word;
            }
            else
            {
                bool flag = false;
                foreach (char c in word)
                {
                    if (!IsVowel(c) && flag != true)
                    {
                        word = word.Substring(1) + c;
                    }
                    else if (IsVowel(c) && flag != true)
                    {
                        flag = true;
                        word += "ay";
                    }
                }
                return word;
            }
        }

        public static string TranslateSentence(string sentence)
        {
            string[] words = sentence.Trim().Split(" ");
            string finalString = "";
            foreach (string word in words)
            {
                string output = Regex.Replace(word, @"[^\w\s]", "");
                output = ReturnWordCapital(output);
                finalString += output + " ";
            }
            return finalString;
        }

        public static string ReturnWordCapital(string word)
        {
            string translation = TranslateWord(word);
            if (char.IsUpper(word[0]))
            {
                return char.ToUpper(translation[0]) + translation.Substring(1);
            }
            else
            {
                return translation;
            }
        }
    }
}