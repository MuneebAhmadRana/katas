﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Katas.Kata10
{
    public class TimeFormat
    {
        public static string GetReadableTime(int sec)
        {
            int secondsLeft;
            //Finding total hours
            int hours = sec / 3600;
            //Seconds left after hours have been calculated
            secondsLeft = sec % 3600;

            //Calculating minutes based on the seconds left after hour conversion
            int minutes = secondsLeft / 60;
            //Remainder is the seconds left.
            int seconds = secondsLeft % 60;

            //Formatting 
            string hoursString = FixFormating(hours);
            string minutesString = FixFormating(minutes);
            string secondsString = FixFormating(seconds);

            return hoursString + ":" + minutesString + ":" + secondsString;
        }

        public static string FixFormating(int value)
        {
            if(value < 10)
            {
                return "0" + value.ToString();
            }
            return value.ToString();
        }
    }
}
