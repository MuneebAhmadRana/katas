﻿namespace Katas.Kata3
{
    internal class Friday13
    {
        public Friday13(int[] years, int count)
        {
            this.years = years;
            this.count = count;
        }

        public int[] years;
        public int count;
    }
}