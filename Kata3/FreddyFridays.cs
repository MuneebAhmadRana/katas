﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Katas.Kata3
{
    internal class FreddyFridays
    {
        public static int[] Friday13s(int year)
        {
            DateTime startDate = new DateTime(year, 1, 1, 0, 0, 0);
            DateTime endDate = new DateTime(year + 1, 1, 1, 0, 0, 0);
            List<DateTime> dateTimeList = new List<DateTime>();
            while (startDate.AddDays(1) <= endDate)
            {
                startDate = startDate.AddDays(1);
                if (startDate.Day.ToString() == "13" && startDate.DayOfWeek.ToString() == "Friday")
                {
                    dateTimeList.Add(startDate);
                }
            }
            Console.WriteLine("There were " + dateTimeList.Count + " times Friday the 13th occured in " + year);
            dateTimeList.ForEach(date => Console.WriteLine(date.ToString("M", CultureInfo.InvariantCulture) + " " + date.Year));
            int[] result = { year, dateTimeList.Count };
            return result;
        }

        public static void Friday13s3000(int year)
        {
            List<Friday13> results = new List<Friday13>();
            while (year + 1 <= 3000)
            {
                year++;
                int[] result = Friday13s(year);
            }
        }
    }
}