﻿using System;
using System.Linq;

namespace Katas.Kata14
{
    public class CreditCardValidator
    {
        public static bool ValidateCard(long cardNumber)
        {
            int cardNumberLength = cardNumber.ToString().Length;
            if (cardNumberLength < 14 || cardNumberLength > 19)
            {
                return false;
            }
            //last digit
            int lastDigit = FindLastDigit(cardNumber);
            //without last digit
            long withoutLast = WithoutLastDigit(cardNumber);
            //reversed
            long reversed = Reversed(withoutLast);
            //reversed doubled at odd positions and added
            int sum = DoubleAndAdd(reversed);
            //last digit of the final sum
            int finalDigit = int.Parse(sum.ToString()[^1..]);
            //final check
            if (10 - finalDigit != lastDigit)
            {
                return false;
            }
            return true;
        }

        //returns the last digit
        public static int FindLastDigit(long cardNumber)
        {
            long lastDigit = cardNumber % 10;
            return (int)lastDigit;
        }

        //returns the card number without the last digit
        public static long WithoutLastDigit(long cardNumber)
        {
            return long.Parse(cardNumber.ToString()[0..^1]);
        }

        //returning the reversed cardnumber without the last digit
        public static long Reversed(long number)
        {
            string numString = number.ToString();
            char[] charArray = numString.ToCharArray();
            Array.Reverse(charArray);
            return long.Parse(new string(charArray));
        }

        //returns the final sum after doubling values in odd number positions
        public static int DoubleAndAdd(long reversed)
        {
            //Going to build on this string
            string final = "";
            string revString = reversed.ToString();
            for (int i = 0; i < revString.Length; i++)
            {
                // all odd ones
                if (i % 2 != 1)
                {
                    char c = revString[i];
                    //doubling
                    int doubled = int.Parse(c.ToString()) * 2;

                    while (doubled >= 10)
                    {
                        doubled = doubled.ToString().Sum(c => c - '0');
                    }
                    final += doubled.ToString();
                }
                else
                {
                    final += revString[i];
                }
            }
            return final.Sum(c => c - '0');
        }
    }
}