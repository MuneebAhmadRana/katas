﻿using System.Collections.Generic;
using System.Linq;

namespace Katas.Kata5
{
    public class RemoveWord
    {
        public static string[] RemoveLetters(string[] array, string word)
        {
            //Converting the array parameter to string List
            List<string> arrayList = array.ToList();
            foreach (char letter in word)
            {
                //Removes the first occurence
                arrayList.Remove(letter.ToString());
            }
            return arrayList.ToArray();
        }
    }
}