﻿using System.Linq;

namespace Katas.Kata15
{
    public class ParselTongue
    {
        public static bool IsParselTongue(string sentence)
        {
            string[] splitSentence = sentence.Trim().ToLower().Split(" ");
            foreach (string word in splitSentence)
            {
                if (word.Contains("s") && !word.Contains("ss")) return false;
            }
            return true;
        }
    }
}